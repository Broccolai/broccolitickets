package co.uk.magmo.broccolitickets.Utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Messenger {

    private static String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "Tickets" + ChatColor.DARK_GRAY + "]";
    private static ChatColor chatColor = ChatColor.AQUA;
    private static ChatColor errorColor = ChatColor.RED;

    public static void playerMessage(Player player, String message, boolean error) {
        if (error) {
            player.sendMessage(prefix + " " + errorColor + message);
        } else {
            player.sendMessage(prefix + " " + chatColor + message);
        }
    }

    public static void consoleMessage(String message) {
        Bukkit.getConsoleSender().sendMessage(prefix + " " + message);
    }
}
