package co.uk.magmo.broccolitickets.Utilities;

import co.uk.magmo.broccolitickets.Objects.Ticket;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Variables {
    // Open tickets HashMap
    private static HashMap<Player, Ticket> tickets = new HashMap<>();

    public static HashMap<Player, Ticket> getTickets() {
        return tickets;
    }

    public static void setTickets(HashMap<Player, Ticket> set) {
        tickets = set;
    }
}
