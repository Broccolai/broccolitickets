package co.uk.magmo.broccolitickets.Utilities;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabCompletion implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] arguments) {
        if (command.getName().equalsIgnoreCase("ticket" ) || command.getName().equalsIgnoreCase("ti" )) {
            if (arguments.length == 1) {
                List<String> list = new ArrayList<>();

                list.add("Create");
                list.add("List");
                list.add("Done");
                list.add("Pick");
                list.add("Yield");
                list.add("Highscore");
                list.add("Modlist");

                Collections.sort(list);

                return list;
            }
        }

        return null;
    }
}
