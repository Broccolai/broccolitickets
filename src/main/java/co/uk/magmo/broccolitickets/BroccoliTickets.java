package co.uk.magmo.broccolitickets;

import static co.uk.magmo.broccolitickets.Utilities.Messenger.consoleMessage;

import co.uk.magmo.broccolitickets.Utilities.TabCompletion;
import org.bukkit.plugin.java.JavaPlugin;

public class BroccoliTickets extends JavaPlugin {
    @Override
    public void onEnable() {
        consoleMessage("Tickets Plugin enabled");

        this.getCommand("ticket").setExecutor(new CommandDispatcher());
        this.getCommand("ti").setExecutor(new CommandDispatcher());
        this.getCommand("ticket").setTabCompleter(new TabCompletion());
        this.getCommand("ti").setTabCompleter(new TabCompletion());
    }

    @Override
    public void onDisable() {
        consoleMessage("Lore Plugin disabled");
    }
}
