package co.uk.magmo.broccolitickets.Commands;

import static co.uk.magmo.broccolitickets.Utilities.Messenger.playerMessage;
import static co.uk.magmo.broccolitickets.Utilities.Variables.*;

import co.uk.magmo.broccolitickets.Objects.Ticket;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;


public class Create {
    private static HashMap<Player, Ticket> tickets = getTickets();

    public static void newTicket(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        Ticket ticket;

        if (args.length <= 1) {
            playerMessage(player, "Please enter a message", true);
            return;
        }

        StringBuilder buildString = new StringBuilder();

        for (int i = 1; i < args.length; i++)
            buildString.append(args[i]).append(" ");

        String message = buildString.toString();

        if (tickets.containsKey(player)) {
            ticket = tickets.get(player);
            Ticket.setMessage(message);
            tickets.replace(player, ticket);
            setTickets(tickets);
        } else {
            ticket = new Ticket();
            Ticket.setMessage(message);
            tickets.put(player, ticket);
            setTickets(tickets);
        }

        playerMessage(player, "Ticket added", false);
    }
}
