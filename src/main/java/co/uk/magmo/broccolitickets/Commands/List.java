package co.uk.magmo.broccolitickets.Commands;

import static co.uk.magmo.broccolitickets.Utilities.Variables.getTickets;

import co.uk.magmo.broccolitickets.Objects.Ticket;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class List {
    private static HashMap<Player, Ticket> tickets = getTickets();

    public static void listTickets(CommandSender sender) {
        Player player = (Player) sender;

        player.sendMessage(ChatColor.GRAY + "------|" + ChatColor.AQUA + " Tickets " + ChatColor.GRAY + "|------");
        tickets.forEach((p, s) ->
            player.sendMessage(ChatColor.GREEN + "# " + ChatColor.WHITE +
                    p.getName() + " - " + ChatColor.GRAY + "" + ChatColor.ITALIC + Ticket.getMessage()));
    }
}
