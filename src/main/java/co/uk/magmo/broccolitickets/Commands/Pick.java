package co.uk.magmo.broccolitickets.Commands;

import co.uk.magmo.broccolitickets.Objects.Ticket;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

import static co.uk.magmo.broccolitickets.Utilities.Variables.getTickets;

public class Pick {
    private static HashMap<Player, Ticket> tickets = getTickets();

    public static void pickTicket(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        Player target = Bukkit.getPlayer(args[1]);
        Ticket ticket;

        if (tickets.containsKey(target)) {
            ticket = tickets.get(target);
            Ticket.setClaimer(player);
            tickets.put(target, ticket);
        } else {

        }
    }
}
