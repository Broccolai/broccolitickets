package co.uk.magmo.broccolitickets.Objects;

import org.bukkit.entity.Player;

import java.util.Date;

public class Ticket {
    //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private static String message;
    private static Player claimer;
    private static Date date = new Date();
    private static boolean picked = false;

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String set) {
        message = set;
    }

    public static Player getClaimer() {
        return claimer;
    }

    public static void setClaimer(Player set) {
        claimer = set;
    }

    public static Date getDate() {
        return date;
    }

    public static boolean isPicked() {
        return picked;
    }

    public static void setPicked(boolean set) {
        picked = set;
    }
}
